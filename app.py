import logging
import csv
import os.path


def format_currency(amount, simbol=""):
    currency = "{:,}".format(amount)
    currency = currency.replace(",", ".")

    return currency


def main():
    x, menu = 0, 0
    nama_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
                  'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    print("Selamat datang di aplikasi pembukuan!!!")
    nama = input("\nMasukan nama user anda: ")
    while x == 0:
        bulan = input("Masukan bulan transaksi: ")
        if bulan in [x.lower() for x in nama_bulan]:
            bulan = bulan.capitalize()
            x = 1
        else:
            x = 0
            print("nama bulan tidak valid\n")

    if os.path.isfile(nama+"_"+bulan+'.csv'):
        with open(nama+"_"+bulan+'.csv') as f:
            mylist = [{k: v for k, v in row.items()}
                      for row in csv.DictReader(f, skipinitialspace=True)]
    else:
        mylist = []

    while x == 1:
        try:
            menu = int(input(
                "\n\n1. Input transaksi baru.\n2. Tampilkan daftar transaksi.\nMasukan nomor menu: "))
            if menu == 1:
                nama_trx = str(input("\nInput nama transaksi: "))
                c_j = 0
                while c_j == 0:
                    try:
                        jumlah_trx = int(input("Input jumlah transaksi: "))
                        c_j = 1 if jumlah_trx > 0 else 0
                        if c_j == 0:
                            print('Jumlah transaksi tidak valid')
                    except Exception as e:
                        logging.error("input harus integer", exc_info=False)
                while c_j == 1:
                    try:
                        tgl_trx = int(input("Input tanggal transaksi: "))
                        # cek tanggal, belum cek tanggal per bulan
                        c_j = 0 if tgl_trx >= 1 and tgl_trx <= 31 else 1
                        if c_j == 1:
                            print('Tanggal transaksi tidak valid')
                    except Exception as e:
                        logging.error("input harus integer", exc_info=False)

                trx = {"tgl": tgl_trx, "nama": nama_trx, "jml": jumlah_trx}
                mylist.append(trx)
                print("Transaksi telah dicatat")
                x = 1
            elif menu == 2:
                total = 0
                print("\n================================================")
                print("Laporan Transaksi Pembukuan si " +
                      nama+" bulan " + bulan)
                print("================================================")
                print("Tanggal | Nama Transaksi | Jumlah")
                print("------------------------------------------------")
                if mylist:
                    for row in mylist:
                        total = total+int(row['jml'])
                        jml = format_currency(int(row['jml']))
                        print(str(row['tgl'])+" | " + row['nama']+" | "+jml)
                    total = format_currency(total)
                    print("------------------------------------------------")
                    print("Total                                    "+total)
                else:
                    print("Belum ada transaksi")
                print("------------------------------------------------")
                submenu = int(
                    input("\nTekan 1 untuk kembali ke menu awal, tekan 2 untuk keluar dari program: "))
                if submenu == 1:
                    x = 1
                elif submenu == 2:
                    keys = mylist[0].keys()
                    with open(nama+"_"+bulan+'.csv', 'w', newline='') as output_file:
                        dict_writer = csv.DictWriter(output_file, keys)
                        dict_writer.writeheader()
                        dict_writer.writerows(mylist)
                    print("Anda telah keluar dari program\n")
                    x = 0
                else:
                    x = 1
                    print("menu tidak valid")
            else:
                x = 1
                print("menu tidak valid")
        except Exception as e:
            logging.error("input harus integer", exc_info=False)


if __name__ == "__main__":
    main()
