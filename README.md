# python-basic-test

For Assessment

## Installation

Important to install Python version >= 3.7
Create a Python 3 virtual environment

```bash
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ _
```

Then, install every necessary python module

```bash
$ pip install -r requirements.txt
```

## Usage

Run your apps:

```bash
(venv) $ python3 app.py
```
